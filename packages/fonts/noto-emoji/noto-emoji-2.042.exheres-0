# Copyright 2020-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

FONT=${PN}

require github [ user=googlefonts tag=v${PV} ] \
    xfont

SUMMARY="Noto Color and Black-and-White emoji fonts"
HOMEPAGE+=" https://fonts.google.com/noto"

LICENCES="OFL-1.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

RESTRICT="test"

DEPENDENCIES=""

src_compile() {
    :
}

src_install() {
    insinto /usr/share/fonts/X11/${PN}
    doins fonts/NotoColorEmoji.ttf

    insinto /usr/share/fontconfig/conf.avail
    doins "${FILES}"/65-noto-color-emoji.conf

    # Enable by default
    dodir /etc/fonts/conf.d
    edo ln -sf /usr/share/fontconfig/conf.avail/65-noto-color-emoji.conf "${IMAGE}"/etc/fonts/conf.d/65-noto-color-emoji.conf

    fix_fonts

    emagicdocs
}

