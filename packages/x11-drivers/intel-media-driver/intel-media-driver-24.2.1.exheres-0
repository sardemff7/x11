# Copyright 2019-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=intel pn=${PN/intel-} tag=${PN/-driver}-${PV} ] \
    cmake

SUMMARY="Intel Media Driver for VAAPI"
DESCRIPTION="
The Intel Media Driver for VAAPI is a new VA-API (Video Acceleration API) user mode driver
supporting hardware accelerated decoding, encoding, and video post processing for GEN based
graphics hardware.
"
HOMEPAGE+=" https://01.org/intel-media-for-linux"

LICENCES="BSD-3 MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/libX11 [[ note = [ X support is automagic ] ]]
    build+run:
        dev-libs/intel-gmmlib[>=22.3.18]
        x11-libs/libva[>=2.21.0][X] [[ note = [ Requires X for va/va_dricommon.h ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_CMRTLIB:BOOL=TRUE
    -DENABLE_KERNELS:BOOL=TRUE
    -DENABLE_NONFREE_KERNELS:BOOL=TRUE
    -DENABLE_PRODUCTION_KMD:BOOL=FALSE
    -DENABLE_XE_KMD:BOOL=TRUE
    -DINSTALL_DRIVER_SYSCONF:BOOL=FALSE
    -DMEDIA_BUILD_FATAL_WARNINGS:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DMEDIA_RUN_TEST_SUITE:BOOL=TRUE -DMEDIA_RUN_TEST_SUITE:BOOL=FALSE'
)

src_prepare() {
    cmake_src_prepare

    # fix etc install to not end up in prefix
    edo sed \
        -e 's:${CMAKE_INSTALL_FULL_SYSCONFDIR}:/etc:g' \
        -i CMakeLists.txt
}

