# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'gtk+-2.13.2.ebuild' from Gentoo which is:
#    Copyright 1999-2008 Gentoo Foundation

require gtk+

SLOT="2"
PLATFORMS="~amd64 ~arm ~x86"

LANGS="af am ang ar as ast az az_IR be be@latin bg bn bn_IN br bs ca
ca@valencia crh cs cy da de dz el en_CA en_GB eo es et eu fa fi fr ga gl gu he
hi hr hu hy ia id io is it ja ka kk kn ko ku li lt lv mai mi mk ml mn mr ms my
nb ne nl nn nso oc or pa pl ps pt pt_BR ro ru rw si sk sl sq sr sr@ije sr@latin
sv ta te th tk tr tt ug uk ur uz uz@cyrillic vi wa xh yi zh_CN zh_HK zh_TW"

MYOPTIONS="
    cups
    gobject-introspection
    gtk-doc
    man-pages [[ description = [ Build and install man pages for the GTK+ API ] ]]
    ( linguas: ${LANGS} )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        x11-proto/xorgproto
        man-pages? ( dev-libs/libxslt
                     app-text/docbook-xml-dtd:4.1.2
                     app-text/docbook-xsl-stylesheets )
        gobject-introspection? ( gnome-desktop/gobject-introspection[>=0.9.3] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.11] )
    build+run:
        dev-libs/glib:2[>=2.28.0]
        dev-libs/atk[>=1.29.2][gobject-introspection?]
        x11-libs/pango[>=1.20][gobject-introspection?]
        x11-libs/cairo[>=1.6][X]
        x11-libs/gdk-pixbuf:2.0[>=2.21.0][gobject-introspection?]
        ( media-libs/fontconfig
          x11-libs/libX11
          x11-libs/libXext
          x11-libs/libXrender
          x11-libs/libXi
          x11-libs/libXrandr[>=1.3]
          x11-libs/libXcursor
          x11-libs/libXfixes
          x11-libs/libXinerama
          x11-libs/libXcomposite
          x11-libs/libXdamage ) [[ note = [ required for the X backend ] ]]
        cups? ( net-print/cups[>=1.2] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
    run:
        x11-themes/hicolor-icon-theme
    suggestion:
        app-vim/gtk-syntax [[ description = [ A collection of vim syntax files for various GTK+ C extensions ] ]]
        gnome-desktop/evince [[ description = [ Used for print preview functionality ] ]]
        x11-themes/breeze-gtk [[
            description = [ Theme to match KDE Plasma's default theme ]
        ]]
"

RESTRICT="test" # requires X

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-Fix-casts.patch
    "${FILES}"/${PNV}-c99.patch
)

# NOTE(compnerd) explicitly override the paths as the prefix path is used for finding the themes
DEFAULT_SRC_CONFIGURE_PARAMS=( --prefix=/usr --exec-prefix=/usr/$(exhost --target)
                               --includedir=/usr/$(exhost --target)/include
                               --with-xinput --with-gdktarget=x11 --disable-papi
                                --enable-xinerama )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'cups' 'gobject-introspection introspection' 'gtk-doc'
                                       'man-pages man' )

src_configure() {
    CC_FOR_BUILD="$(exhost --target)-cc"                  \
    PKG_CONFIG_FOR_BUILD="$(exhost --target)-pkg-config"  \
    default
}

src_install() {
    default
    gtk+_icon_cache_alternatives
    edo find "${IMAGE}" -type d -empty -delete
    edo echo 'gtk-fallback-icon-theme = "gnome"' > "${IMAGE}/usr/share/gtk-2.0/gtkrc"
}
