# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg [ suffix=tar.xz ]

SUMMARY="X Toolkit Intrinsics library"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

# fails Alloc test, last checked: 1.3.0
RESTRICT="test"

DEPENDENCIES="
    build:
        x11-proto/xorgproto
        x11-utils/util-macros[>=1.19.0-r1] [[
            note = [ Introduced a patch for XORG_PROG_RAWCPP to avoid the use of unprefixed cpp ]
        ]]
    build+run:
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
    test:
        dev-libs/glib:2[>=2.40]
"

src_configure() {
    local build=$(exhost --build)
    local build_cflags_var=${build//-/_}_CFLAGS
    local build_cppflags_var=${build//-/_}_CPPFLAGS
    local build_ldflags_var=${build//-/_}_LDFLAGS

    local myconfig=(
        # for crosscompiling
        --disable-malloc0returnsnull
        --disable-static
        --enable-xkb
        $(expecting_tests && echo '--enable-unit-tests' || echo '--disable-unit-tests')
    )

    CC_FOR_BUILD="$(exhost --build)-cc"         \
    CFLAGS_FOR_BUILD="${!build_cflags_var}"     \
    CPPFLAGS_FOR_BUILD="${!build_cppflags_var}" \
    LDFLAGS_FOR_BUILD="${!build_ldflags_var}"   \
    econf "${myconfig[@]}"
}

