# Copyright 2016-2018, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtWebSockets module"
DESCRIPTION="
WebSocket is a web-based protocol designed to enable two-way communication
between a client application and a remote host. It enables the two entities
to send data back and forth if the initial handshake succeeds. WebSocket is
the solution for applications that struggle to get real-time data feeds with
less network latency and minimum data exchange.

The Qt WebSockets module provides C++ and QML interfaces that enable Qt
applications to act as a server that can process WebSocket requests, or a
client that can consume data received from the server, or both."

MYOPTIONS="examples
    qml [[ description = [ Support for QtQuick and the QML language ] ]]
"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
        qml? ( x11-libs/qtdeclarative:${SLOT}[>=${PV}] )
"

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'examples QT_BUILD_EXAMPLES'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'qml Qt6Quick'
    'qml Qt6QuickTest'
)

qtwebsockets-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtwebsockets-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

