# Copyright 2014 Jorge Aparicio
# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Wayland plugin for Qt"
HOMEPAGE="http://qt-project.org/wiki/${PN}"

LICENCES+="
    GPL-3
    HPND [[ note = [ wayland-protocol, wayland-txt-input-unstable ] ]]
    MIT  [[ note = [ wayland-{ivi-extension-protocol,xdg-shell-protocol} ] ]]
"
MYOPTIONS="
    examples
    qml [[ description = [ Support for QtQuick and the QML language ] ]]
    vulkan [[ description = [ Support for Vulkan-based server buffer integration ] ]]

    examples [[ requires = qml ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        vulkan? ( sys-libs/vulkan-headers )
    build+run:
        sys-libs/wayland[>=1.8.0]
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/libxkbcommon[>=0.5.0]
        x11-libs/qtbase:${SLOT}[>=${PV}][gui]
        qml? ( x11-libs/qtdeclarative:${SLOT}[>=${PV}] )
"

if ever at_least 6.7.0-rc ; then
    DEPENDENCIES+="
        build+run:
            x11-libs/qtbase:${SLOT}[>=${PV}][wayland]
    "
fi

qtwayland-6_src_configure() {
    local cmake_params=(
        # I don't see where this is used after the removal of the
        # xcomposite-{glx,egl} plugins, so disable searching for it.
        -DCMAKE_DISABLE_FIND_PACKAGE_X11:BOOL=TRUE
        $(cmake_option examples QT_BUILD_EXAMPLES)
        $(cmake_disable_find qml Qt6Qml)
        $(cmake_disable_find qml Qt6Quick)
        $(qt_cmake_feature vulkan wayland_vulkan_server_buffer)
    )

    ecmake "${cmake_params[@]}"
}

qtwayland-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtwayland-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

