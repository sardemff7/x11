# Copyright 2018, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtLocation, QtPositioning"
DESCRIPTION="
The Qt Positioning API gives developers the ability to determine a position by
using a variety of possible sources, including satellite, or wifi, or text
file, and so on. That information can then be used to for example determine a
position on a map. In addition satellite information can be retrieved and area
based monitoring can be performed.

The Qt Location API enables you to:
- access and present map data,
- support touch gesture on a specific area of the map,
- query for a specific geographical location and route,
- add additional layers on top, such as polylines and circles,
- and search for places and related images.
"

LICENCES="FDL-1.3 GPL-2 GPL-3 LGPL-3"
MYOPTIONS="
    examples
    nmea [[ description = [ Plugin which parses NMEA sentences into position updates ] ]]
    gps [[ description = [ Use the Gypsy daemon to provide satellite information ] ]]
    qml [[ description = [ Support for QtQuick and the QML language ] ]]
"

DEPENDENCIES="
    build:
        gps? ( virtual/pkg-config )
    build+run:
        dev-libs/icu:=
        sys-libs/zlib
        x11-libs/qtbase:${SLOT}[>=${PV}]
        examples? ( x11-libs/qtsvg:${SLOT}[>=${PV}] )
        gps? (
            gnome-platform/GConf:2
            gps/gypsy
        )
        nmea? ( x11-libs/qtserialport:${SLOT}[>=${PV}] )
        qml? ( x11-libs/qtdeclarative:${SLOT}[>=${PV}] )
"

qtpositioning_src_configure() {
    local cmake_params=(
        $(cmake_option examples QT_BUILD_EXAMPLES)
        $(qt_cmake_feature gps gypsy)
        $(cmake_disable_find examples Qt6Svg)
        $(cmake_disable_find nmea Qt6SerialPort)
        $(cmake_disable_find qml Qt6Qml)
        $(cmake_disable_find qml Qt6Quick)
        $(cmake_disable_find qml Qt6QuickTest)
    )

    cmake_src_configure "${cmake_params[@]}"
}

qtpositioning_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtpositioning_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

