# Copyright 2022 Calvin Walton <calvin.walton@kepstin.ca>
# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'cairo-1.6.4.ebuild' from Gentoo which is:
#     Copyright 1999-2008 Gentoo Foundation# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>

require xdummy [ phase=test option=X ]
require meson

export_exlib_phases src_configure src_test

SUMMARY="Cairo is a 2D graphics library"
DESCRIPTION="
Cairo is a vector graphics library with cross-device output support. It
currently supports the X Window System and in-memory image buffers as output
targets. It is designed to produce identical output on all output media while
taking advantage of display hardware acceleration when available (eg. through
the X Render Extension). It provides a stateful user-level API with capabilities
similar to the PDF 1.4 imaging model and provides operations including stroking
and filling Bezier cubic splines, transforming and compositing translucent
images, and antialiased text rendering.
"
HOMEPAGE="https://www.cairographics.org"
DOWNLOADS="${HOMEPAGE}/releases/${PNV}.tar.xz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/documentation/"

LICENCES="LGPL-2.1 MPL-1.1"
SLOT="0"
MYOPTIONS="X gtk-doc"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc )
        X? ( x11-proto/xorgproto )
    build+run:
        app-arch/lzo:2 [[ note = [ automagic ] ]]
        dev-libs/expat
        dev-libs/glib:2[>=2.14]
        media-libs/fontconfig[>=2.2.95]
        media-libs/freetype:2[>=2.12.0] [[ note = [ SVG, COLRv1 font support ] ]]
        media-libs/libpng:=[>=1.2.8]
        sys-libs/zlib
        x11-libs/pixman:1[>=0.36.0]
        X? (
            x11-libs/libX11
            x11-libs/libxcb[>=1.6]
            x11-libs/libXext
            x11-libs/libXrender[>=0.6]
        )
    test:
        app-text/ghostscript [[ note = [ required for ps ] ]]
        app-text/libspectre[>=0.2.0] [[ note = [ required for building ps tests ] ]]
        app-text/poppler[glib][>=0.17.4] [[ note = [ required for building pdf tests ] ]]
        gnome-desktop/librsvg:2[>=2.35.0] [[ note = [ required for building svg tests ] ]]
"

cairo_src_configure() {
    local -a exmeson_args

    # Be careful when enabling expermental or unsupported backends
    #   - Hard disable unsupported xlib xcb backend
    #     This backend is incomplete and causes rendering problems
    exmeson_args=(
        # Cairo font backends
        -Dfontconfig=enabled
        -Dfreetype=enabled
        # Cairo surface backends
        -Dpng=enabled
        -Dtee=enabled
        -Dxlib-xcb=disabled
        -Dzlib=enabled
        # Misc deps
        -Dglib=enabled

        $(meson_feature X xcb)
        $(meson_feature X xlib)
        $(meson_switch gtk-doc gtk_doc)
        $(expecting_tests -Dtests=enabled -Dtests=disabled)
        $(expecting_tests -Dspectre=enabled -Dspectre=disabled)
    )

    if ! exhost --is-native -q; then
        edo cat > cairo-meson-cross.ini <<EOF
[properties]
ipc_rmid_deferred_release = 'true'
EOF
        exmeson_args+=( --cross-file cairo-meson-cross.ini )
    fi

    exmeson "${exmeson_args[@]}"
}

# known to be broken upstream
RESTRICT="test"

cairo_src_test() {
    # TODO: they have slow tests marked; figure out how to disable them or run them only when
    # expensive_tests is enabled.
    esandbox allow_net unix:"${WORK}"/test/.any2ppm
    optionq X && xdummy_start
    meson_src_test
    optionq X && xdummy_stop
}

