# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require xorg meson

if ever is_scm ; then
    SCM_REPOSITORY="https://anongit.freedesktop.org/git/${PN}.git"
    require scm-git
else
    DOWNLOADS="https://www.x.org/archive/individual/${XORG_DIRECTORY}/${PNV}.tar.xz"
fi

SUMMARY="Pixman is a library that provides low-level pixel manipulation"

LICENCES="MIT"
SLOT="1"
MYOPTIONS="
    arm_cpu_features: iwmmxt iwmmxt2 neon simd
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    test:
        media-libs/libpng:=
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddemos=disabled
    -Dgnu-inline-asm=enabled
    -Dgnuplot=false
    -Dgtk=disabled
    -Dloongson-mmi=disabled
    -Dmips-dspr2=disabled
    -Dopenmp=disabled
    -Dtimers=false
    -Dtls=enabled
    -Dvmx=disabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    arm_cpu_features:iwmmxt
    arm_cpu_features:neon
    'arm_cpu_features:simd arm-simd'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    arm_cpu_features:iwmmxt2
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dlibpng=enabled -Dlibpng=disabled'
    '-Dtests=enabled -Dtests=disabled'
)

